// Aggregate to count the total number of items supplied by Yellow Farms
// and has a price less than 50. ($count stage)
db.fruits.aggregate([
    {
        $match: {
            $and: [{ supplier: "Yellow Farms" }, { price: { $lt: 50 } }],
        },
    },
    { $count: "yellowFarmLowerThan50" },
]);

// Aggregate to count the total number of items with price lesser than 30.
// ($count stage)
db.fruits.aggregate([
    {
        $match: {
            $and: [{ price: { $lt: 30 } }],
        },
    },
    { $count: "priceLesserThan30" },
]);

// Aggregate to get the average price of fruits supplied by Yellow Farm.
// ($group)
db.fruits.aggregate([
    { $match: { supplier: "Yellow Farms" } },
    { $group: { _id: "Yellow Farms", AvgPrice: { $avg: "$price" } } },
]);

// Aggregate to get the highest price of fruits supplied by Red Farm Inc.
// ($group)
db.fruits.aggregate([
    { $match: { supplier: "Red Farms Inc." } },
    { $group: { _id: "Red Farms Inc.", maxPrice: { $max: "$price" } } },
]);

// Aggregate to get the lowest price of fruits supplied by Red Farm Inc.
// ($group)
db.fruits.aggregate([
    { $match: { supplier: "Red Farms Inc." } },
    { $group: { _id: "Red Farms Inc.", minPrice: { $min: "$price" } } },
]);
